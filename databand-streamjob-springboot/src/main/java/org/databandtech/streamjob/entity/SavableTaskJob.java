package org.databandtech.streamjob.entity;

import java.sql.ResultSet;

import org.databandtech.streamjob.sink.SinkFunction;


public interface SavableTaskJob<T> {
	

	String SaveData(ResultSet rs, SinkFunction sink); 

}
