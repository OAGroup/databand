package org.databandtech.streamjob;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatabandStreamjobSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatabandStreamjobSpringbootApplication.class, args);
	}

}
