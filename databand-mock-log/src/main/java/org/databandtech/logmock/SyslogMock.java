package org.databandtech.logmock;

import java.util.Date;

import org.databandtech.common.Mock;
import org.productivity.java.syslog4j.Syslog;
import org.productivity.java.syslog4j.SyslogConstants;
import org.productivity.java.syslog4j.SyslogIF;

/**
 * syslog（udp）生成，运行后可以用flume进行测试，配置文件见：/flumeConf/syslog-log.properties：
 * @author Administrator
 *
 */
public class SyslogMock {

	static final String HOST = "127.0.0.1";
	static final int PORT = 9099;
	static final int COUNT = 1000;


	public void generate() {
		SyslogIF syslog = Syslog.getInstance(SyslogConstants.UDP);
		syslog.getConfig().setHost(HOST);
		syslog.getConfig().setPort(PORT);

		for (int i = 0; i < COUNT; i++) {
			StringBuffer buffer = new StringBuffer();
			buffer.append("时间:" + new Date().toString().substring(4, 20) + ";")
					.append("姓名:" + Mock.getChineseName() + ";").append("数量:" + Mock.getNumString(100) + ";");

			try {
				syslog.log(0, buffer.toString());
			} catch (Exception e) {
				System.out.println("generate log get exception " + e);
			}
		}
		System.out.println("log 已发送.");

	}

	public static void main(String[] args) {
		new SyslogMock().generate();
	}

}
