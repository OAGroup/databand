package org.databandtech.logmock;

import java.util.ArrayList;
import java.util.List;

import org.databandtech.common.Mock;
import org.databandtech.logmock.entity.ShowLog;
import org.databandtech.logmock.utils.Csv;

/**
 * 节目播出日志
 */
public class ShowsCSVLog {

	static int YEAR = 2020;
	static int[] MONTHS = {10,11,12};
	static int COUNTLOW = 100000;//每天最少的订单数
	static int COUNTHIGH = 200000;//每天最多的订单数，每天的订单数会在两者之间随机
	static String[] CHANNELIDS = {"电影","电视剧","综艺","少儿","纪录片"};
	static String[] MOVIES = {"哪吒","叶问","八佰","夏洛特烦恼","一点就到家","邪不压正","狄仁杰","决战中途岛","美女与野兽"};
	static String[] TVS = {"亲爱的麻洋街","将军家的小娘子","破茧","瞄准","追梦","在一起","都很好"};
	static String[] CHILDS = {"猪猪侠","恐龙玩具酷","旺旺队立大功","熊出没","小猪佩奇","钢铁飞龙"};
	static String[] VARIETIES = {"奋斗吧主播","火星情报局","这！就是街舞","一站到底","王牌对王牌","我们恋爱吧"};
	static String[] DOCUS = {"记忆的力量","味道中原","日食记","被点亮的星球","军武次位面","飞跃地球"};
	static String[] STATUS = {"tv_playing","vod_playing","browsing","tvod_playing","ad_event","external_link","order"};
	static String[] CITYS = {"北京","北京","北京","上海","上海","上海","广州","广州","深圳","深圳","重庆","杭州","武汉","南京","郑州","西安","成都","长沙"};
	static String FILE_PATH = "c:\\logs\\csv\\show\\";


	public static void main(String[] args) {
		
		for (int month : MONTHS) {
			for (int day = 1;day<31;day++) {
				makeLogByDay("电影",month,day);
				makeLogByDay("电视",month,day);				
				makeLogByDay("综艺",month,day);				
				makeLogByDay("少儿",month,day);				
				makeLogByDay("纪录片",month,day);				
			}
		}
	}


	private static void makeLogByDay(String channelId, int month, int day) {
		List<ShowLog> list = new ArrayList<ShowLog>();
		int randomCount = Mock.getNum(COUNTLOW, COUNTHIGH);
		for(int i=0;i<randomCount;i++) {
			ShowLog show = new ShowLog();
			show.setAreaCode("-");
			show.setCityCode(CITYS[Mock.getNum(0, CITYS.length-1)]);
			show.setChannelId(channelId);
			show.setColumnId("-");
			show.setHd(Mock.getNum(0, 4));
			show.setShowDateTime(YEAR+"-"+month+"-"+day);
			show.setShowDuration(Mock.getNum(0, 200));
			if (channelId =="电影") {
				int indexShow = Mock.getNum(0, MOVIES.length-1);
				show.setShowId(MOVIES[indexShow]);				
			}
			if (channelId =="电视") {
				int indexShow = Mock.getNum(0, TVS.length-1);
				show.setShowId(TVS[indexShow]);				
			}
			if (channelId =="综艺") {
				int indexShow = Mock.getNum(0, VARIETIES.length-1);
				show.setShowId(VARIETIES[indexShow]);				
			}
			if (channelId =="少儿") {
				int indexShow = Mock.getNum(0, CHILDS.length-1);
				show.setShowId(CHILDS[indexShow]);				
			}
			if (channelId =="纪录片") {
				int indexShow = Mock.getNum(0, DOCUS.length-1);
				show.setShowId(DOCUS[indexShow]);				
			}
			show.setStatus(STATUS[Mock.getNum(0, STATUS.length-1)]);
			show.setUserId(Mock.getCnName());
			
			list.add(show);
		}
		
		String path = FILE_PATH + channelId +"-" + YEAR +"-" +"-"+month+"-"+day + ".csv";
		Csv.writeCSV(list, path);
	}

}
