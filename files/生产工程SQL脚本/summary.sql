SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `summary`
-- ----------------------------
DROP TABLE IF EXISTS `summary`;
CREATE TABLE `summary` (
  `id` bigint(20) NOT NULL,
  `org` varchar(100) DEFAULT NULL,
  `dept` varchar(100) DEFAULT NULL,
  `group` varchar(100) DEFAULT NULL,
  `key` varchar(100) DEFAULT NULL,
  `valueint` int(11) DEFAULT NULL,
  `valuedecemal` decimal(10,0) DEFAULT NULL,
  `bussdate` varchar(20) DEFAULT NULL,
  `insertdate` varchar(20) DEFAULT NULL,
  `ext` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `keyindex` (`key`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of summary
-- ----------------------------
