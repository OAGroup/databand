package org.databandtech.job.service;

import java.util.List;

import org.databandtech.job.entity.ScheduledBean;

public interface ScheduledService {
	
	List<ScheduledBean> getAllSchedule();

    Boolean start(String jobcode);

    Boolean stop(String jobcode);

    Boolean restart(String jobcode);

    void startAllSchedule(List<ScheduledBean> scheduledTaskBeanList);

}
